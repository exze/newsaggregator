//
//  AppDelegate.swift
//  NewsAggregator
//
//  Created by Egor Pankratov on 10/21/20.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    
    func application(_ application: UIApplication,
                     didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        self.window = UIWindow(frame: UIScreen.main.bounds)
        self.window?.rootViewController = UINavigationController(
            rootViewController: NAMainViewController())
        self.window?.makeKeyAndVisible()
        
        return true
    }

}

