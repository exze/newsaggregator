//
//  NANewsTableViewCell.swift
//  NewsAggregator
//
//  Created by Egor Pankratov on 10/22/20.
//

import UIKit
import SnapKit

class NANewsTableViewCell: UITableViewCell {
    
    // MARK: - Variables
    
    static private(set) var reuseIdentifier = "NANewsTableViewCell"
    
    private var isDescriptionExpanded: Bool = false
    
    var onDescriptionLabelExpanded: ((Bool) -> Void)?
    
    // MARK: - GUI variables
    
    private let wrapperViewEdgeInsets = UIEdgeInsets(top: 15, left: 10, bottom: 2, right: 10)
    private let sourceLabelEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 10)
    private let titleLabelEdgeInsets = UIEdgeInsets(top: 0, left: 10, bottom: 10, right: 10)
    private let descriptionLabelEdgeInsets = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
    
    private lazy var wrapperView: UIView = {
        let view = UIView()
        view.backgroundColor = NAColorConstants.cellBackgroundColor
        view.layer.cornerRadius = 10.0
        view.clipsToBounds = true
        view.isUserInteractionEnabled = true
        view.addGestureRecognizer(self.tapGestureRecognizer)
        return view
    }()
    
    private(set) lazy var previewImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.alpha = 0
        return imageView
    }()
    
    private(set) lazy var sourceNameLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "Helvetica", size: 11.5)
        label.textColor = NAColorConstants.titleColor
        return label
    }()
    
    private(set) lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "Helvetica-Bold", size: 14.0)
        
        label.textColor = NAColorConstants.titleColor
        label.numberOfLines = 0
        return label
    }()
    
    private(set) lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "Helvetica", size: 12.5)
        label.numberOfLines = 0
        label.textColor = NAColorConstants.descriptionColor
        return label
    }()
    
    private(set) lazy var dateTimeLabel: UILabel = {
       let label = UILabel()
        label.font = UIFont(name: "Helvetica", size: 11.5)
        label.textColor = NAColorConstants.secondaryTextColor
        return label
    }()
    
    private(set) lazy var viewStateLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "Helvetica", size: 11.5)
        label.textColor = NAColorConstants.secondaryTextColor
        label.text = "Просмотрено | "
        label.isHidden = true
        return label
    }()
    
    private(set) lazy var tapGestureRecognizer: UITapGestureRecognizer = {
        let gestureRecognizer = UITapGestureRecognizer()
        gestureRecognizer.addTarget(self, action: #selector(self.touchCell))
        return gestureRecognizer
    }()
    
    // MARK: - Init
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.selectionStyle = .none
        
        self.contentView.addSubview(self.wrapperView)
        
        self.wrapperView.addSubview(self.previewImageView)
        self.wrapperView.addSubview(self.sourceNameLabel)
        self.wrapperView.addSubview(self.titleLabel)
        self.wrapperView.addSubview(self.descriptionLabel)
        self.wrapperView.addSubview(self.dateTimeLabel)
        self.wrapperView.addSubview(self.viewStateLabel)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()

        self.previewImageView.alpha = 0
        self.descriptionLabel.alpha = 1
    }
    
    // MARK: - Constraints
    
    override func updateConstraints() {
        
        self.wrapperView.snp.updateConstraints { (make) in
            make.edges.equalToSuperview().inset(self.wrapperViewEdgeInsets)
        }
        
        self.sourceNameLabel.snp.updateConstraints { (make) in
            make.centerY.equalTo(self.wrapperView.snp.top).offset(12)
            make.right.equalToSuperview().inset(self.sourceLabelEdgeInsets)
        }
        
        self.previewImageView.snp.remakeConstraints { (make) in
            make.left.right.equalToSuperview()
            
            if self.previewImageView.isHidden {
                make.top.equalTo(self.sourceNameLabel.snp.bottom)
                make.height.equalTo(0)
            } else {
                make.top.equalTo(self.sourceNameLabel.snp.bottom).offset(8)
                make.height.equalTo(self.previewImageView.snp.width).multipliedBy(2.0/3.0)
            }
        }
        
        self.titleLabel.snp.updateConstraints { (make) in
            make.top.equalTo(self.previewImageView.snp.bottom).offset(10)
            make.left.right.equalToSuperview().inset(self.titleLabelEdgeInsets)
        }
        
        self.setDescriptionLabelConstraints()
        
        self.dateTimeLabel.snp.remakeConstraints { (make) in
            make.right.equalToSuperview().offset(-10)
            make.centerY.equalTo(self.wrapperView.snp.bottom).offset(-12)
        }
        
        self.viewStateLabel.snp.updateConstraints { (make) in
            make.right.equalTo(self.dateTimeLabel.snp.left)
            make.centerY.equalTo(self.dateTimeLabel)
        }
        
        super.updateConstraints()
    }
    
    private func setDescriptionLabelConstraints() {
        self.descriptionLabel.snp.remakeConstraints { (make) in
            make.top.equalTo(self.titleLabel.snp.bottom).offset(10)
            make.left.right.equalToSuperview().inset(self.descriptionLabelEdgeInsets)
            make.bottom.equalTo(self.dateTimeLabel.snp.top)
                .offset(self.isDescriptionExpanded ? -10 : 0)
            if !self.isDescriptionExpanded {
                make.height.equalTo(0)
            }
        }
    }
    
    // MARK: - Methods
    
    func set(newsModel: NANewsModel, loadingHandler: (() -> Void)? = nil) {
        if let _ = newsModel.imageUrl {
            self.previewImageView.isHidden = false
        } else {
            self.previewImageView.isHidden = true
        }
        
        if let image = newsModel.previewImage {
            self.previewImageView.image = image
            UIView.animate(withDuration: 0.5) {
                self.previewImageView.alpha = 1
                self.previewImageView.layoutIfNeeded()
            }
        }
        
        self.titleLabel.text = newsModel.title
        self.descriptionLabel.text = newsModel.description
        self.sourceNameLabel.text = "Источник: \(newsModel.sourceName)"
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd.MM.yyyy  HH:mm"
        self.dateTimeLabel.text = dateFormatter.string(from: newsModel.publicationDate)
        self.viewStateLabel.isHidden = !newsModel.isViewed
        
        self.isDescriptionExpanded = newsModel.isExpanded
        self.setNeedsUpdateConstraints()
    }
    
    // MARK: - Actions
    
    @objc func touchCell() {
        self.isDescriptionExpanded = !self.isDescriptionExpanded
        if self.isDescriptionExpanded {
            self.viewStateLabel.isHidden = false
        }
        
        self.setDescriptionLabelConstraints()
        UIView.animate(withDuration: 0.3) {
            self.descriptionLabel.alpha = self.isDescriptionExpanded ? 1 : 0
            self.layoutIfNeeded()
        }
        
        self.onDescriptionLabelExpanded?(self.isDescriptionExpanded)
    }
}
