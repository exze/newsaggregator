//
//  NAMainView.swift
//  NewsAggregator
//
//  Created by Egor Pankratov on 10/21/20.
//

import UIKit

class NAMainView: UIView {
    
    // MARK: - GUI variables
    
    private let noDataLabelEdgeInsets = UIEdgeInsets(top: 0, left: 40, bottom: 0, right: 40)
    
    private(set) lazy var newsTableView: UITableView = {
        let tableView = UITableView()
        tableView.tableFooterView = UIView(frame: .zero)
        tableView.separatorStyle = .none
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 350
        return tableView
    }()
    
    private(set) lazy var noDataLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.text = "Данные отсутствуют. Проверьте интернет-соединение и перезагрузите страницу"
        label.textAlignment = .center
        label.font = UIFont(name: "Helvetica", size: 16)
        label.textColor = NAColorConstants.secondaryTextColor
        label.isHidden = true
        return label
    }()
    
    private(set) lazy var activityIndicatorView: UIActivityIndicatorView = {
        let activityIndicatorView = UIActivityIndicatorView(style: .medium)
        activityIndicatorView.hidesWhenStopped = true
        activityIndicatorView.tintColor = NAColorConstants.titleColor
        return activityIndicatorView
    }()
    
    // MARK: - Init
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.addSubview(self.newsTableView)
        self.addSubview(self.noDataLabel)
        self.addSubview(self.activityIndicatorView)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Constraints
    
    override func updateConstraints() {
        self.newsTableView.snp.updateConstraints { (make) in
            make.edges.equalToSuperview()
        }
        self.activityIndicatorView.snp.updateConstraints { (make) in
            make.centerX.equalToSuperview()
            make.bottom.equalTo(self.noDataLabel.snp.top).offset(-8)
        }
        self.noDataLabel.snp.updateConstraints { (make) in
            make.centerY.equalToSuperview()
            make.left.right.equalToSuperview().inset(self.noDataLabelEdgeInsets)
        }
        super.updateConstraints()
    }
}
