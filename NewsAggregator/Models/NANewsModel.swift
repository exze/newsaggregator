//
//  NANewsModel.swift
//  NewsAggregator
//
//  Created by Egor Pankratov on 10/22/20.
//

import UIKit
import Alamofire

class NANewsModel {
    
    // MARK: - Variables
    
    var imageUrl: String?
    var title: String = ""
    var description: String = ""
    var publicationDate: Date = Date()
        
    var previewImage: UIImage?
    var sourceName: String = ""
    var isExpanded: Bool = false
    var isViewed: Bool = false
    
    // MARK: - Init
    
    init() { }
    
    init(responseModel: NANewsResponseModel) {
        self.imageUrl = responseModel.imageUrl
        self.title = responseModel.title
        self.description = responseModel.description
        self.publicationDate = responseModel.pubDate
    }
    
    // MARK: - Methods
    
    func downloadImage(completionHandler: ((UIImage?) -> Void)?) {
        guard let imageUrl = self.imageUrl else { return }
        AF.request(imageUrl).response { response in
            switch(response.result) {
            case .success(let data):
                guard let data = data,
                      let image = UIImage(data: data) else { return }
                self.previewImage = image
                completionHandler?(image)
            case .failure(let error):
                debugPrint(error.localizedDescription)
                completionHandler?(nil)
            }
        }
    }
}
