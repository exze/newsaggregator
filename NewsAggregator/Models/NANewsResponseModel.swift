//
//  NANewsResponseModel.swift
//  NewsAggregator
//
//  Created by Egor Pankratov on 10/24/20.
//

import Foundation

class NANewsResponseModel: Codable {
    
    // MARK: - Variables
    
    var imageUrl: String?
    var title: String
    var description: String
    var pubDate: Date
    
    // MARK: - Init
    
    init(imageUrl: String? = nil, title: String, description: String, pubDate: Date) {
        self.title = title
        self.description = description
        self.pubDate = pubDate
        self.imageUrl = imageUrl
    }
    
    required convenience init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        let imageUrl = try? container.decode(String.self, forKey: .imageUrl)
        let title = try container.decode(String.self, forKey: .title)
        let description = try container.decode(String.self, forKey: .description)
        
        let dateString = try container.decode(String.self, forKey: .pubDate)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "E, d MMM yyyy HH:mm:ss Z"
        let pubDate = dateFormatter.date(from: dateString) ?? Date()
        
        self.init(imageUrl: imageUrl, title: title, description: description, pubDate: pubDate)
    }
}
