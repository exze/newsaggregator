//
//  NAXMLParserDelegate.swift
//  NewsAggregator
//
//  Created by Egor Pankratov on 10/26/20.
//

import Foundation

class NAFeedXMLParser: XMLParser, XMLParserDelegate {

    // MARK: - Variables

    private var results: [[String: String]]?
    private var currentDictionary: [String: String]?
    private var currentValue: String?
    var completionHandler: (([[String: String]]?) -> Void)?
    
    // MARK: - Init
    
    override init(data: Data) {
        super.init(data: data)
        self.delegate = self
    }
    
    // MARK: - Methods

    func parserDidStartDocument(_ parser: XMLParser) {
        self.results = []
    }

    func parser(_ parser: XMLParser, didStartElement elementName: String,
                namespaceURI: String?, qualifiedName qName: String?,
                attributes attributeDict: [String : String]) {
        switch elementName {
        case "item":
            self.currentDictionary = [:]
        case "title", "description", "pubDate":
            self.currentValue = ""
        case "enclosure":
            self.currentDictionary?["imageUrl"] = attributeDict["url"]
        default:
            break
        }
    }

    func parser(_ parser: XMLParser, foundCharacters string: String) {
        self.currentValue? += string
    }

    func parser(_ parser: XMLParser, didEndElement elementName: String,
                namespaceURI: String?, qualifiedName qName: String?) {
        switch elementName {
        case "item":
            self.results?.append(self.currentDictionary!)
            self.currentDictionary = nil
        case "title", "description", "pubDate":
            self.currentDictionary?[elementName] = self.currentValue?
                .trimmingCharacters(in: [" ", "\n", "\r"])
            self.currentValue = nil
        default:
            break
        }
    }
    
    func parserDidEndDocument(_ parser: XMLParser) {
        self.completionHandler?(self.results)
    }
    
    func parser(_ parser: XMLParser, parseErrorOccurred parseError: Error) {
        debugPrint(parseError)
        self.currentValue = nil
        self.currentDictionary = nil
        self.results = nil
    }
}
