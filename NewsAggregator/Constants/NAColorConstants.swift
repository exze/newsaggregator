//
//  NAColorConstants.swift
//  NewsAggregator
//
//  Created by Egor Pankratov on 10/23/20.
//

import UIKit

class NAColorConstants {
    static let cellBackgroundColor = UIColor.init(white:  240.0/256.0, alpha: 1)
    static let titleColor = UIColor(white:  30.0/256.0, alpha: 1)
    static let descriptionColor = UIColor(white: 80.0/256.0, alpha: 1)
    static let secondaryTextColor = UIColor(white: 180.0/256.0, alpha: 1)
}
