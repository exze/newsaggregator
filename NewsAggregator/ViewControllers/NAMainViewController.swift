//
//  ViewController.swift
//  NewsAggregator
//
//  Created by Egor Pankratov on 10/21/20.
//

import UIKit
import Alamofire

class NAMainViewController: UIViewController {

    // MARK: - Variables
    
    private let newsSources: [String: String] = [
        "Mail.ru": "https://news.mail.ru/rss/belarus/",
        "Lenta.ru": "https://lenta.ru/rss",
        "Газета.Ru": "https://www.gazeta.ru/export/rss/lenta.xml"
    ]
       
    private var newsList: [NANewsModel] = []
    
    private var isConnectedToInternet: Bool {
        return NetworkReachabilityManager()?.isReachable ?? false
    }
    
    // MARK: - GUI variables
    
    private(set) lazy var contentView: NAMainView = {
        let view = NAMainView()
        view.newsTableView.register(
            NANewsTableViewCell.self,
            forCellReuseIdentifier: NANewsTableViewCell.reuseIdentifier)
        view.newsTableView.dataSource = self
        view.newsTableView.refreshControl = self.refreshControl
        return view
    }()
    
    private(set) lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(
            self,
            action: #selector(self.dragRefreshControl),
            for: .valueChanged)
        return refreshControl
    }()
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = "Новости"
        self.view.backgroundColor = .white
        self.view.addSubview(self.contentView)
                
        self.updateConstraints()
        self.contentView.activityIndicatorView.startAnimating()
        
        self.startDataLoading { [weak self] in
            guard let self = self else { return }
            UIView.animate(withDuration: 0.5) {
                self.contentView.activityIndicatorView.alpha = 0
                self.contentView.activityIndicatorView.layoutIfNeeded()
            } completion: { _ in
                self.contentView.newsTableView.reloadData()
                self.contentView.activityIndicatorView.stopAnimating()
            }
        }
    }
    
    // MARK: - Constraints
    
    private func updateConstraints() {
        self.contentView.snp.updateConstraints { (make) in
            make.edges.equalTo(self.view.safeAreaLayoutGuide)
        }
    }
    
    // MARK: - Methods
    
    private func startDataLoading(completionHandler: (() -> Void)?) {
        var newsList = [NANewsModel]()
                
        if self.isConnectedToInternet {
            self.contentView.noDataLabel.isHidden = true
            let dispatchGroup = DispatchGroup()
            for (sourceName, url) in self.newsSources {
                dispatchGroup.enter()
                self.loadNewsData(from: url, sourceName) { news, _ in
                    if let news = news {
                        newsList += news
                    }
                    dispatchGroup.leave()
                }
            }
            
            dispatchGroup.notify(queue: DispatchQueue.main) { [weak self] in
                guard let self = self else { return }
                newsList.sort { $0.publicationDate > $1.publicationDate }
                self.newsList = newsList
                completionHandler?()
            }
        } else {
            self.contentView.noDataLabel.isHidden = false
            self.newsList = newsList
            completionHandler?()
        }
    }
    
    private func loadNewsData(from url: String, _ sourceName: String,
                              completionHandler: (([NANewsModel]?, Error?) -> Void)? = nil) {
        
        AF.request(url).response { response in
            switch (response.result) {
            case .success(let data):
                guard let data = data else { return }
                let xmlParser = NAFeedXMLParser(data: data)
                xmlParser.completionHandler = { result in
                    guard let items = result else { return }
                    var news = [NANewsModel]()
                    let decoder = JSONDecoder()
                    for item in items {
                        guard let data = try? JSONSerialization
                                .data(withJSONObject: item, options:.prettyPrinted),
                              let responseModel = try? decoder
                                .decode(NANewsResponseModel.self, from: data)
                        else { continue }
                        
                        let newsModel = NANewsModel(responseModel: responseModel)
                        newsModel.sourceName = sourceName
                        news.append(newsModel)
                    }
                    completionHandler?(news, nil)
                }
                xmlParser.parse()
            case .failure(let error):
                debugPrint(error.localizedDescription)
                completionHandler?(nil, error)
            }
        }
    }
    
    // MARK: - Actions
    
    @objc func dragRefreshControl() {
        self.startDataLoading { [weak self] in
            guard let self = self else { return }
            self.contentView.newsTableView.reloadData()
            self.refreshControl.endRefreshing()
        }
    }
}

// MARK: - TableView DataSource extension

extension NAMainViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.newsList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(
            withIdentifier: NANewsTableViewCell.reuseIdentifier,
            for: indexPath) as? NANewsTableViewCell else { return UITableViewCell() }
        
        let model = self.newsList[indexPath.row]
        
        cell.onDescriptionLabelExpanded = { isExpanded in
            model.isExpanded = isExpanded
            if isExpanded {
                model.isViewed = true
            }
            tableView.beginUpdates()
            tableView.endUpdates()
        }
        
        if model.previewImage == nil, let _ = model.imageUrl {
            model.downloadImage { (image) in
                guard let indexPaths = tableView.indexPathsForVisibleRows,
                      indexPaths.contains(indexPath),
                      let cell = tableView.cellForRow(at: indexPath) as? NANewsTableViewCell
                else { return }
                cell.previewImageView.image = image
                UIView.animate(withDuration: 0.3) {
                    cell.previewImageView.alpha = 1
                    cell.previewImageView.layoutIfNeeded()
                }
            }
        }
        cell.set(newsModel: model)
        return cell
    }
}
